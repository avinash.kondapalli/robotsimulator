import java.util.Objects;

public class Location {
    protected Position position;
    protected Direction direction;

    public Location(Position position, Direction direction) {
        this.position = position;
        this.direction = direction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Location location = (Location) o;
        return Objects.equals(position, location.position) &&
                direction == location.direction;
    }

    @Override
    public int hashCode() {
        return Objects.hash(position, direction);
    }
}
