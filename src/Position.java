import java.util.Objects;

//job if the class
public class Position {
    protected int xCoordinate;
    protected int yCoordinate;
    private int advanceX;
    private int advanceY;


    Position(int xCoordinate, int yCoordinate) {
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
    }

    public Position advance(Direction direction) {
        switch (direction) {
            case EAST:
                advanceX = 1;
                advanceY = 0;
                break;
            case NORTH:
                advanceX = 0;
                advanceY = 1;
                break;
            case SOUTH:
                advanceX = 0;
                advanceY = -1;
                break;
            case WEST:
                advanceX = -1;
                advanceY = 0;
        }
        return new Position(this.xCoordinate + advanceX, this.yCoordinate + advanceY);

    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return xCoordinate == position.xCoordinate &&
                yCoordinate == position.yCoordinate;
    }

    @Override
    public int hashCode() {
        return Objects.hash(xCoordinate, yCoordinate);
    }
}